package net.moose.crucian.network.client;

import junit.framework.TestCase;

import net.moose.crucian.network.domain.AnglePacket;
import net.moose.crucian.network.domain.AuthRequest;
import net.moose.crucian.network.domain.AuthResponse;
import net.moose.crucian.network.domain.DiscoveryPacket;
import net.moose.crucian.network.domain.MovePacket;
import net.moose.crucian.network.domain.ShotPacket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class AllPacketsSendingTest extends TestCase
{
    public byte[] toByteArray(Serializable serializable) throws IOException
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;
        out = new ObjectOutputStream(bos);
        out.writeObject(serializable);
        return bos.toByteArray();
    }

    public void testSending() throws Exception
    {
        DatagramPacket pack;
        byte[] obj;
        DatagramSocket sock = new DatagramSocket();
        sock.setBroadcast(true);

        AnglePacket angle = new AnglePacket(45);
        AuthRequest request = new AuthRequest();
        request.Name = "Test";
        AuthResponse response = new AuthResponse();
        response.ServerName = "TestServer";
        DiscoveryPacket discoveryPacket = new DiscoveryPacket();
        MovePacket move = new MovePacket(128);
        ShotPacket shot = new ShotPacket(10);

        obj = toByteArray(angle);
        pack = new DatagramPacket(obj, obj.length, InetAddress.getByName("255.255.255.255"), 8080);
        sock.send(pack);

        obj = toByteArray(request);
        pack = new DatagramPacket(obj, obj.length, InetAddress.getByName("255.255.255.255"), 8080);
        sock.send(pack);
        obj = toByteArray(response);
        pack = new DatagramPacket(obj, obj.length, InetAddress.getByName("255.255.255.255"), 8080);
        sock.send(pack);
        obj = toByteArray(discoveryPacket);
        pack = new DatagramPacket(obj, obj.length, InetAddress.getByName("255.255.255.255"), 8080);
        sock.send(pack);
        obj = toByteArray(move);
        pack = new DatagramPacket(obj, obj.length, InetAddress.getByName("255.255.255.255"), 8080);
        sock.send(pack);
        obj = toByteArray(shot);
        pack = new DatagramPacket(obj, obj.length, InetAddress.getByName("255.255.255.255"), 8080);
        sock.send(pack);
        sock.close();
    }
}
