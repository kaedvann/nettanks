package net.moose.crucian.network.client;

import net.moose.crucian.network.domain.AnglePacket;
import net.moose.crucian.network.domain.AuthRequest;
import net.moose.crucian.network.domain.AuthResponse;
import net.moose.crucian.network.domain.INetWorker;
import net.moose.crucian.network.domain.MovePacket;
import net.moose.crucian.network.domain.PingPacket;
import net.moose.crucian.network.domain.ShotPacket;
import net.moose.crucian.network.domain.eventargs.AnglePacketEventArgs;
import net.moose.crucian.network.domain.eventargs.MovePacketEventArgs;
import net.moose.crucian.network.domain.eventargs.ShotPacketEventArgs;
import net.moose.crucian.utils.ActionResult;
import net.moose.crucian.utils.ExceptionEventArgs;
import net.moose.crucian.utils.NetLogger;

import org.npospelt.events.EventArgs;
import org.npospelt.events.EventHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Класс, инкапсулирующий в себе работу с сетевыми API Андроида и предоставляющий простой интерфейс для подключения
 * к другому устройству, отправки сведений и получения уведомлений от другого устройства
 */
public class NetworkClient implements INetWorker
{
    /**
     * объект для журналировани
     */
    private final Logger _logger = LoggerFactory.getLogger(NetworkClient.class);

    /**
     * Поток для получения входящих сообщений
     */
    private ExecutorService _listeningThread = Executors.newSingleThreadExecutor();

    /**
     * Поток, в котором выполняется обработка всех событий
     */
    private ExecutorService _eventThread = Executors.newSingleThreadExecutor();

    /**
     * Поток для отправки сообщений
     */
    private ExecutorService _writingThread = Executors.newSingleThreadExecutor();

    /**
     * Сокет, через который осуществляется все сетевое взаимодействие
     */
    private Socket _clientSocket;

    /**
     * Объект, сериализующий переданные ему пакеты и записывающий их в выходной поток сокета
     */
    private ObjectOutputStream _outputStream;

    /**
     * Указывает, подключен ли клиент к другому устройству
     */
    private boolean _connected = false;

    /**
     * Объект, выполняющий чтение из входного потока и десериализацию полученных байтов в объекты
     */
    private ObjectInputStream _inputStream;

    /**
     * Имя сетевого клиента
     */
    private String _name;

    /**
     * Имя устройства, к которому подключен клиент
     */
    private String _serverName = "unknown";

    /**
     * Одноразовый код, используемый для подключения
     */
    private int _connectionCode;

    /**
     * Событие подключения
     */
    private EventHolder<EventArgs> _connectedEvent = new EventHolder<>();

    /**
     * Событие получения пакета с информацией о перемещении соперника
     */
    private EventHolder<MovePacketEventArgs> _movePacketReceivedEvent = new EventHolder<>();

    /**
     * Событие ошибки
     */
    private EventHolder<ExceptionEventArgs<Exception>> _errorOccurred = new EventHolder<>();

    /**
     * Событие ошибки подключения
     */
    private EventHolder<EventArgs> _connectionFailed = new EventHolder<>();

    /**
     * Событие, возникающее при попытке подключения с некорректным кодом
     */
    private EventHolder<EventArgs> _authorizationFailed = new EventHolder<>();

    /**
     * Событие отключения
     */
    private EventHolder<EventArgs> _disconnected = new EventHolder<>();

    /**
     * Событие получения пакеты с информацией об изменении угла пушки соперника
     */
    private EventHolder<AnglePacketEventArgs> _anglePacketReceived = new EventHolder<>();

    /**
     * Событие выстрела соперника
     */
    private EventHolder<ShotPacketEventArgs> _shotPacketReceived = new EventHolder<>();

    //region event invokers
    /**
     * Объект для вызова обработчиков события успешной авторизации
     */
    private Runnable _authorizationConfirmedInvoker = new Runnable()
    {
        @Override
        public void run()
        {
            _connectedEvent.raiseEvent(this, EventArgs.emptyEventArgs);
        }
    };

    /**
     * Объект для вызова обработчиков события проваленной авторизации
     */
    private Runnable _authorizationFailedInvoker = new Runnable()
    {
        @Override
        public void run()
        {
            _authorizationFailed.raiseEvent(this, EventArgs.emptyEventArgs);
        }
    };

    /**
     * Объект для вызова обработчиков события отключения
     */
    private Runnable _disconnectedInvoker = new Runnable()
    {
        @Override
        public void run()
        {
            _disconnected.raiseEvent(this, EventArgs.emptyEventArgs);
            _errorOccurred.raiseEvent(this, new ExceptionEventArgs<>(new Exception()));
        }
    };
    //endregion

    /**
     * Конструктор сетевого клиента
     */
    private NetworkClient()
    {
    }

    /**
     * Фабричный метод для создания клиента
     *
     * @return Сетевой клиент с настройками по умолчанию
     * @throws IOException, если недоступны сетевые сервисы
     */
    public static NetworkClient createClient() throws IOException
    {
        return new NetworkClient();
    }

    /**
     * @return Событие получения информации о выстреле соперника
     */
    @Override
    public EventHolder<ShotPacketEventArgs> getShotPacketReceivedEvent()
    {
        return _shotPacketReceived;
    }


    /**
     * @return Событие получения информации о движении соперника
     */
    public EventHolder<MovePacketEventArgs> getMovePacketReceivedEvent()
    {
        return _movePacketReceivedEvent;
    }


    /**
     * @return Событие отключения
     */
    @Override
    public EventHolder<EventArgs> getDisconnectedEvent()
    {
        return _disconnected;
    }


    /**
     * @return Событие получения информации об изменении угла пушки соперника
     */
    @Override
    public EventHolder<AnglePacketEventArgs> getAnglePacketReceivedEvent()
    {
        return _anglePacketReceived;
    }

    /**
     * Отправляет пакет с информацией о движении подключенному устройству
     * @param dx Перемещение танка
     */
    @Override
    public void sendMovePacket(final float dx)
    {
        sendPacket(new MovePacket(dx));
    }

    /**
     * Обобщенный метод для отправки всех пакетов
     * @param packet Пакет для отправки
     */
    private void sendPacket(final Serializable packet)
    {
        _writingThread.submit(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    _outputStream.writeObject(packet);
                }
                catch (final IOException e)
                {
                    e.printStackTrace();
                    handleDisconnect(e);
                }
            }
        });
    }

    /**
     * Обрабатывает отключение от другого устройства
     * @param e Исключение, вызвавшее потерю соединения
     */
    private void handleDisconnect(final Exception e)
    {
        dispose();
        NetLogger.log(_name, "Проверка соединения", ActionResult.Error);
        _logger.error("connection failed", e);
        _eventThread.submit(new Runnable()
        {
            @Override
            public void run()
            {
                _disconnected.raiseEvent(NetworkClient.this, new ExceptionEventArgs<>(e));
            }
        });
    }

    /**
     * @return Имя сетевого клиента
     */
    public String getName()
    {
        return _name;
    }

    /**
     * @return Имя соперника
     */
    @Override
    public String getEnemyName()
    {
        return _serverName;
    }

    /**
     * Освобождает неуравляемые ресурсы, используемые сетевым клиентом
     */
    @Override
    public void dispose()
    {
        try
        {
            _connected = false;
            if (_outputStream != null) _outputStream.close();
            if (_inputStream != null) _inputStream.close();
            if (_clientSocket != null) _clientSocket.close();
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
        }
    }

    /**
     * Отправляет пакет с информацией о выстреле
     * @param power Мощность выстрела
     */
    @Override
    public void sendShotPacket(float power)
    {
        sendPacket(new ShotPacket(power));
    }

    /**
     * Отправляет пакет с информацией об изменении угла пушки
     * @param angle Новый угол
     */
    @Override
    public void sendAnglePacket(float angle)
    {
        sendPacket(new AnglePacket(angle));
    }

    /**
     * Устанавливает имя сетевого клиента
     * @param name
     */
    public void setName(final String name)
    {
        _name = name;
    }

    /**
     * @return Событие неудачной авторизации
     */
    public EventHolder<EventArgs> getAuthorizationFailedEvent()
    {
        return _authorizationFailed;
    }

    /**
     * @return Событие ошибки подключения
     */
    public EventHolder<EventArgs> getConnectionFailedEvent()
    {
        return _connectionFailed;
    }

    /**
     * Устанавливает соединение с указанными параметрами
     * @param host Адрес для подключения
     * @param port Порт для подключения
     */
    public void connect(final InetAddress host, final int port)
    {
        _listeningThread.execute(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    _clientSocket = new Socket();
                    _clientSocket.setTcpNoDelay(true);
                    _clientSocket.connect(new InetSocketAddress(host, port), 10 * 1000);
                    _outputStream = new ObjectOutputStream(_clientSocket.getOutputStream());
                    _inputStream = new ObjectInputStream(_clientSocket.getInputStream());
                    _connected = true;
                    sendAuthorization();

                    listenIncoming();
                }
                catch (final IOException e)
                {
                    NetLogger.log(_name, "Подключение к серверу", ActionResult.Error);
                    _logger.error("connection error", e);
                    _eventThread.submit(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            ExceptionEventArgs<Exception> args = new ExceptionEventArgs<>();
                            args.Exception = e;
                            _errorOccurred.raiseEvent(this, args);
                        }
                    });
                }
            }
        });
    }

    /**
     * Отправляет запрос на авторизацию
     * @throws IOException При ошибке сети
     */
    private void sendAuthorization() throws IOException
    {
        AuthRequest req = new AuthRequest();
        req.Name = _name;
        req.Code = _connectionCode;
        _outputStream.writeObject(req);
        _outputStream.flush();
        _logger.info("authorization request sent");
        NetLogger.log(_name, "Отправление запроса на авторизацию", ActionResult.Success);
    }

    /**
     * Слушает входящие сообщения на сокете и оповещает о них подписчиков
     */
    private void listenIncoming()
    {
        while (IsConnected())
        {
            try
            {
                Object packet = _inputStream.readObject();
                if (packet instanceof AuthResponse) handleAuthResponse((AuthResponse) packet);
                if (packet instanceof MovePacket) handleMovePacket((MovePacket) packet);
                if (packet instanceof AnglePacket) handleAnglePacket((AnglePacket) packet);
                if (packet instanceof ShotPacket) handleShotPacket((ShotPacket) packet);
            }
            catch (ClassNotFoundException | OptionalDataException e)
            {
                _logger.error("error parsing incoming packet");
            }
            catch (Exception e)
            {
                handleDisconnect(e);
            }
        }
    }

    /**
     * @return true, сли установлено подключение, false иначе
     */
    private boolean IsConnected()
    {
        return _connected;
    }

    /**
     * Обрабатывает ответ на запрос авторизации
     * @param packet Ответ на запрос
     */
    private void handleAuthResponse(AuthResponse packet)
    {
        if (packet.IsSuccessful)
        {
            _serverName = packet.ServerName;

            _logger.info("authorized");
            NetLogger.log(_name, "Авторизация", ActionResult.Success);
            _eventThread.submit(_authorizationConfirmedInvoker);
        }
        else
        {
            NetLogger.log(_name, "Авторизация", ActionResult.Error);
            _logger.error("authorization failed");
            _eventThread.submit(_authorizationFailedInvoker);
        }
    }

    /**
     * Обрабатывает полученное сообщение о движении
     * @param packet Сообщение
     */
    private void handleMovePacket(final MovePacket packet)
    {
        NetLogger.log(_serverName, "Движение", ActionResult.Success);
        _eventThread.submit(new Runnable()
        {
            @Override
            public void run()
            {
                _movePacketReceivedEvent.raiseEvent(this, new MovePacketEventArgs(packet));
            }
        });
    }


    /**
     * Обрабатывает полученное сообщение об изменении угла
     * @param packet Сообщение
     */
    private void handleAnglePacket(final AnglePacket packet)
    {
        NetLogger.log(_serverName, "Изменение угла", ActionResult.Success);
        _eventThread.submit(new Runnable()
        {
            @Override
            public void run()
            {
                _anglePacketReceived.raiseEvent(this, new AnglePacketEventArgs(packet));
            }
        });
    }


    /**
     * Обрабатывает полученное сообщение о выстреле
     *
     * @param packet Сообщение
     */
    private void handleShotPacket(final ShotPacket packet)
    {
        NetLogger.log(_serverName, "Выстрел", ActionResult.Success);
        _eventThread.submit(new Runnable()
        {
            @Override
            public void run()
            {
                _shotPacketReceived.raiseEvent(this, new ShotPacketEventArgs(packet));
            }
        });
    }

    /**
     * Проверяет связь с устройством
     */
    private void ping()
    {
        sendPacket(new PingPacket());
    }

    /**
     * @return Событие подключения
     */
    public EventHolder<EventArgs> getConnectedEvent()
    {
        return _connectedEvent;
    }

    /**
     * @return Событие ошибки
     */
    public EventHolder<ExceptionEventArgs<Exception>> getErrorOccurredEvent()
    {
        return _errorOccurred;
    }

    /**
     * @return Код подключения
     */
    public int getConnectionCode()
    {
        return _connectionCode;
    }

    /**
     * Устанавливает указанный код подключения
     * @param connectionCode Код для использования при подключении
     */
    public void setConnectionCode(int connectionCode)
    {
        _connectionCode = connectionCode;
    }
}
