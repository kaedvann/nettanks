package net.moose.crucian.network.client;

import org.npospelt.events.EventArgs;

import java.net.InetAddress;

/**
 * Created by Crucian on 08.10.2014.
 */
public class BroadcastServerEventArgs extends EventArgs
{
    public String Name;
    public InetAddress Address;

    public BroadcastServerEventArgs(final String message, final InetAddress address)
    {
        Name = message;
        Address = address;
    }
}
