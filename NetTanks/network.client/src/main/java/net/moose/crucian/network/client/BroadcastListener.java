package net.moose.crucian.network.client;

import android.content.Context;
import android.net.wifi.WifiManager;

import net.moose.crucian.network.domain.DiscoveryPacket;
import net.moose.crucian.utils.ActionResult;
import net.moose.crucian.utils.ExceptionEventArgs;
import net.moose.crucian.utils.NetLogger;

import org.npospelt.events.EventHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;
import java.nio.ByteOrder;
import java.nio.channels.DatagramChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by Crucian on 08.10.2014.
 */
public class BroadcastListener
{
    private final Context _context;
    private String _name;
    private final List<InetAddress> _addresses = Collections.synchronizedList(new ArrayList<InetAddress>());
    private final Logger _logger = LoggerFactory.getLogger(BroadcastListener.class);
    private ExecutorService _executor = Executors.newSingleThreadExecutor();
    private boolean _isActive;

    private EventHolder<BroadcastServerEventArgs> _broadcastServerDiscovered = new EventHolder<>();
    private EventHolder<ExceptionEventArgs<Exception>> _errorOccurred = new EventHolder<>();
    private DatagramSocket _socket;

    private BroadcastListener(Context context, String name)
    {
        _context = context;
        _name = name;
    }

    public static BroadcastListener createListener(Context context, String name)
    {
        return new BroadcastListener(context, name);
    }


    public void start()
    {
        _isActive = true;
        _executor.submit(new Runnable()
        {
            @Override
            public void run()
            {
                discoverLoop();
            }
        });
    }

    private void discoverLoop()
    {
        WifiManager wifi = (WifiManager) _context.getSystemService(Context.WIFI_SERVICE);
        WifiManager.MulticastLock lock = wifi.createMulticastLock("lock");
        try
        {
            _socket = new DatagramSocket(null);
            _socket.setReuseAddress(true);

             _socket.bind(new InetSocketAddress(InetAddress.getByName("0.0.0.0"), 6667));
            _socket.setBroadcast(true);
            byte[] data = new byte[1024];
            DatagramPacket packet = new DatagramPacket(data, data.length);

            lock.acquire();
            _socket.setSoTimeout(10 * 1000);
           _socket.receive(packet);
                Object pack = parsePacket(packet.getData());
                if (pack instanceof DiscoveryPacket)
                {
                    DiscoveryPacket discoveryPacket = (DiscoveryPacket) pack;
                    _addresses.add(packet.getAddress());
                    _broadcastServerDiscovered.raiseEvent(this, new BroadcastServerEventArgs(discoveryPacket.Message, packet.getAddress()));
                }



        }
        catch (SocketTimeoutException exc)
        {
            _isActive = false;
            NetLogger.log(_name, "Поиск сервера", ActionResult.Error);
            _errorOccurred.raiseEvent(this, new ExceptionEventArgs<>(new Exception()));
        }
        catch (Exception e)
        {
            _isActive = false;
            _logger.error("datagram error", e);
            e.printStackTrace();
        }
        finally
        {
            lock.release();
            if (_socket != null) _socket.close();
        }
    }

    public synchronized boolean isActive()
    {
        return _isActive;
    }

    private Object parsePacket(final byte[] data)
    {
        try
        {
            ObjectInputStream str = new ObjectInputStream(new ByteArrayInputStream(data));
            Object packet = str.readObject();
            return packet;
        }
        catch (Exception e)
        {
            _logger.warn("coudn't parse broadcast packet", e);
        }
        return new Object();
    }

    public void stop()
    {
        _isActive = false;
        _executor.shutdownNow();
        _executor = Executors.newSingleThreadExecutor();
    }

    public EventHolder<BroadcastServerEventArgs> getBroadcastServerDiscovered()
    {
        return _broadcastServerDiscovered;
    }

    public EventHolder<ExceptionEventArgs<Exception>> getErrorOccurred()
    {
        return _errorOccurred;
    }
}
