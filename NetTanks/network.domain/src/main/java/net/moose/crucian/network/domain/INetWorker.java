package net.moose.crucian.network.domain;

import net.moose.crucian.network.domain.eventargs.AnglePacketEventArgs;
import net.moose.crucian.network.domain.eventargs.MovePacketEventArgs;
import net.moose.crucian.network.domain.eventargs.ShotPacketEventArgs;

import org.npospelt.events.EventArgs;
import org.npospelt.events.EventHolder;

public interface INetWorker
{
    EventHolder<ShotPacketEventArgs> getShotPacketReceivedEvent();

    EventHolder<MovePacketEventArgs> getMovePacketReceivedEvent();

    EventHolder<EventArgs> getDisconnectedEvent();

    EventHolder<AnglePacketEventArgs> getAnglePacketReceivedEvent();

    void sendMovePacket(float dx);

    String getName();

    String getEnemyName();

    void dispose();

    void sendShotPacket(float power);

    void sendAnglePacket(float cannonAngle);
}
