package net.moose.crucian.network.domain;

import java.io.Serializable;

/**
 * Сообщение, содержащее новый угол пушки
 */
public class AnglePacket implements Serializable
{
    /**
     * Угол
     */
    public float Angle;

    /**
     * Конструктор
     * @param angle Угол для передачи
     */
    public AnglePacket(float angle)
    {
        Angle = angle;
    }
}
