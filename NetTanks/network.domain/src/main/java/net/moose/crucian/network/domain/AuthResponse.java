package net.moose.crucian.network.domain;

import java.io.Serializable;

/**
 * Сообщение ответа на запрос авторизации
 */
public class AuthResponse implements Serializable
{
    /**
     * Признак успеха
     */
    public boolean IsSuccessful = false;

    /**
     * Имя сервера
     */
    public String ServerName;
}
