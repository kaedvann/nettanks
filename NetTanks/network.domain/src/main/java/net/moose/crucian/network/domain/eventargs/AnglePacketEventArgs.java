package net.moose.crucian.network.domain.eventargs;

import net.moose.crucian.network.domain.AnglePacket;

import org.npospelt.events.EventArgs;

public class AnglePacketEventArgs extends EventArgs
{
    public final AnglePacket Packet;

    public AnglePacketEventArgs(AnglePacket packet)
    {
        Packet = packet;
    }
}
