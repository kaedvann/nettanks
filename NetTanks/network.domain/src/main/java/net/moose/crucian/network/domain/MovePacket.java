package net.moose.crucian.network.domain;

import java.io.Serializable;

/**
 * Сообщение с информацией о движении
 */
public class MovePacket implements Serializable
{
    /**
     * Перемещение
     */
    public float dx;

    /**
     * Конструктор
     * @param dx Перемещение для передачи
     */
    public MovePacket(float dx)
    {
        this.dx = dx;
    }
}
