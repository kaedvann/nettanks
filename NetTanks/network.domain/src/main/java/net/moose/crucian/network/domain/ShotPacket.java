package net.moose.crucian.network.domain;

import java.io.Serializable;

/**
 * Сообщение с информацией о выстреле
 */
public class ShotPacket implements Serializable
{
    /**
     * Мощность выстрела
     */
    public float Power;

    /**
     * Конструктор
     * @param power Мощность выстрела
     */
    public ShotPacket(float power)
    {
        Power = power;
    }
}
