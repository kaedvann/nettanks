package net.moose.crucian.network.domain;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Сообщение для обнаружение сервера
 */
public class DiscoveryPacket implements Serializable
{
    /**
     * Строка приветствия
     */
    public String Message = "hello";

    /**
     * Преобразует сообщение в массив байтов
     * @return Сообщение, записанное в массив байтов
     */
    public byte[] getData()
    {
        try
        {
            ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
            ObjectOutputStream str = new ObjectOutputStream(bytestream);
            str.writeObject(this);
            str.flush();
            return bytestream.toByteArray();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return new byte[0];
    }
}
