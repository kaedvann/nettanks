package net.moose.crucian.network.domain.eventargs;

import net.moose.crucian.network.domain.ShotPacket;

import org.npospelt.events.EventArgs;

public class ShotPacketEventArgs extends EventArgs
{
    public ShotPacket Packet;

    public ShotPacketEventArgs(ShotPacket packet)
    {
        Packet = packet;
    }
}
