package net.moose.crucian.network.domain;

import java.io.Serializable;

/**
 * Сообщение запроса на авторизацию
 */
public class AuthRequest implements Serializable
{
    /**
     * Имя пользователя
     */
    public String Name;

    /**
     * Код подключения
     */
    public int Code = 0x11223344;
}
