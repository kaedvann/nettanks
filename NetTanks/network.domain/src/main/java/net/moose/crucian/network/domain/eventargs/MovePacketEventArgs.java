package net.moose.crucian.network.domain.eventargs;

import net.moose.crucian.network.domain.MovePacket;

import org.npospelt.events.EventArgs;

public class MovePacketEventArgs extends EventArgs
{
    public MovePacket MovePacket;

    public MovePacketEventArgs(MovePacket packet)
    {
        MovePacket = packet;
    }
}
