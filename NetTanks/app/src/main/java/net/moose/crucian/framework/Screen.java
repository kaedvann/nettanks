package net.moose.crucian.framework;

/**
 * Класс, представляющий собой абстракцию игрового экрана
 */
public abstract class Screen
{
    /**
     * Экземпляр игры, которой принадлежит данный экран
     */
    protected final Game game;

    /**
     * Конструктор
     * @param game Игра, управляющая созданным экраном
     */
    public Screen(Game game)
    {
        this.game = game;
    }

    /**
     * Метод, в котором происходит обработка пользовательского ввода
     * @param deltaTime Промежуток времени с последнего обновления
     */
    public abstract void update(float deltaTime);

    /**
     * Метод, в котором происходит формирование изображения
     * @param deltaTime Промежуток времени с последнего обновления
     */
    public abstract void present(float deltaTime);

    /**
     * метод для приостановки игры
     */
    public abstract void pause();

    /**
     * Метод для продолжения приостановленной игры
     */
    public abstract void resume();

    /**
     * Метод для освобождения неуправляемых ресурсов, занятых экраном
     */
    public abstract void dispose();
}
