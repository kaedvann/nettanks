package net.moose.crucian.tanks;

import android.os.Bundle;

import net.danlew.android.joda.JodaTimeAndroid;
import net.moose.crucian.framework.Screen;
import net.moose.crucian.framework.impl.GLGame;
import net.moose.crucian.utils.ActionResult;
import net.moose.crucian.utils.NetLogger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class TanksGame extends GLGame
{
    boolean firstTimeCreate = true;
    private Logger _logger = LoggerFactory.getLogger(TanksGame.class);

    @Override
    public Screen getStartScreen()
    {
        return new MainMenuScreen(this);
    }

    @Override
    public void onCreate(Bundle savedInstance)
    {
        super.onCreate(savedInstance);
        JodaTimeAndroid.init(this);
        NetLogger.log(Settings.login, "Создание главного меню", ActionResult.Success);
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException(Thread thread, Throwable ex)
            {
                _logger.error("bdysh", ex);
            }
        });
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        super.onSurfaceCreated(gl, config);
        if (firstTimeCreate)
        {
            Settings.load(getFileIO());
            Assets.load(this);
            firstTimeCreate = false;
        } else
        {
            Assets.reload();
        }
    }
}