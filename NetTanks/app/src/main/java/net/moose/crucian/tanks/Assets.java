package net.moose.crucian.tanks;

import net.moose.crucian.framework.FileIO;
import net.moose.crucian.framework.gl.Font;
import net.moose.crucian.framework.gl.Texture;
import net.moose.crucian.framework.gl.TextureRegion;
import net.moose.crucian.framework.impl.GLGame;

public class Assets
{
    public static Texture background, gameScreen, wait_background, settings, help;
    public static TextureRegion backgroundRegion, gameRegion, waitRegion, settingsRegion, helpRegion;
    public static FileIO fileIO;
    public static Texture items;
    public static TextureRegion mainMenu;
    public static TextureRegion pauseMenu;
    public static TextureRegion ready;
    public static TextureRegion gameOver;
    public static TextureRegion connectRegion;
    public static TextureRegion logo;
    public static TextureRegion arrowLeft;
    public static TextureRegion arrowRight;
    public static TextureRegion pause;
    public static TextureRegion cannon;
    public static TextureRegion cannonEnemy;
    public static TextureRegion tank;
    public static TextureRegion tankEnemy;
    public static TextureRegion charge;
    public static TextureRegion healthBar;
    public static TextureRegion powerBar;
    public static TextureRegion cannonAngle;
    public static Font font;

    public static void load(GLGame game)
    {
        background = new Texture(game, "background.png");
        settings = new Texture(game, "settingsmenu.png");
        wait_background = new Texture(game, "wait_background.png");
        help = new Texture(game, "help.png");
        backgroundRegion = new TextureRegion(background, 0, 0, 1920, 1080);
        waitRegion = new TextureRegion(wait_background, 0, 0, 1920, 1080);
        settingsRegion = new TextureRegion(settings, 0, 0, 1920, 1080);
        helpRegion = new TextureRegion(help, 0, 0, 1920, 1080);
        gameScreen = new Texture(game, "gamescreen.png");
        gameRegion = new TextureRegion(gameScreen, 0, 0, 1920, 1080);
        items = new Texture(game, "items.png");
        mainMenu = new TextureRegion(items, 504, 165, 580, 558);
        charge = new TextureRegion(items, 392, 91, 28, 28);
        pauseMenu = new TextureRegion(items, 564, 757, 360, 113);
        ready = new TextureRegion(items, 320, 224, 192, 32);
        gameOver = new TextureRegion(items, 567, 883, 326, 125);
        logo = new TextureRegion(items, 1115, 40, 497, 232);
        arrowLeft = new TextureRegion(items, 13, 795, 193, 124);
        arrowRight = new TextureRegion(items, 229, 784, 193, 124);
        pause = new TextureRegion(items, 564, 757, 360, 113);
        tank = new TextureRegion(items, 34, 160, 280, 229);
        tankEnemy = new TextureRegion(items, 34, 382, 280, 229);
        cannon = new TextureRegion(items, 327, 240, 146, 56);
        cannonEnemy = new TextureRegion(items, 326, 453, 146, 56);
        cannonAngle = new TextureRegion(items, 42, 618, 371, 140);
        font = new Font(items, 224, 0, 16, 16, 20);
        healthBar = new TextureRegion(items, 42, 29, 308, 37);
        powerBar = new TextureRegion(items, 42, 87, 308, 37);

    }

    public static void reload()
    {
        background.reload();
        items.reload();
    }

}
