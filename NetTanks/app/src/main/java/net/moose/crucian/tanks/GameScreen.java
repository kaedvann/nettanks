package net.moose.crucian.tanks;

import android.widget.Toast;

import net.moose.crucian.framework.Input.TouchEvent;
import net.moose.crucian.framework.gl.Camera2D;
import net.moose.crucian.framework.gl.FPSCounter;
import net.moose.crucian.framework.gl.SpriteBatcher;
import net.moose.crucian.framework.impl.GLScreen;
import net.moose.crucian.framework.math.Circle;
import net.moose.crucian.framework.math.OverlapTester;
import net.moose.crucian.framework.math.Rectangle;
import net.moose.crucian.framework.math.Vector2;
import net.moose.crucian.network.domain.INetWorker;
import net.moose.crucian.network.domain.eventargs.AnglePacketEventArgs;
import net.moose.crucian.network.domain.eventargs.MovePacketEventArgs;
import net.moose.crucian.network.domain.eventargs.ShotPacketEventArgs;
import net.moose.crucian.tanks.World.WorldListener;
import net.moose.crucian.utils.ActionResult;
import net.moose.crucian.utils.NetLogger;

import org.npospelt.events.EventArgs;
import org.npospelt.events.IEventHandler;

import java.util.List;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Crucian on 25.10.2014.
 */
public class GameScreen extends GLScreen
{
    static final int GAME_READY = 0;
    static final int GAME_RUNNING = 1;
    static final int GAME_PAUSED = 2;
    static final int GAME_LEVEL_END = 3;
    static final int GAME_SHOT = 4;
    static final int GAME_OVER = 5;
    private final TanksGame _game;
    private final INetWorker _netWorker;

    int state;
    Camera2D guiCam;
    Vector2 touchPoint;
    SpriteBatcher batcher;
    World world;
    WorldListener worldListener;
    WorldRenderer renderer;
    Rectangle pauseBounds, moveRightBounds, moveLeftBounds, powerBounds;
    Circle pleeBounds;
    Rectangle resumeBounds, cannonAngleBounds;
    Rectangle quitBounds;
    int lastScore, _tank;
    String scoreString;
    Vector2 cannonCenter = new Vector2(198, 963);
    FPSCounter fpsCounter;
    boolean touchLeft = false, touchRight = false;

    public GameScreen(final TanksGame game, int tank, INetWorker netWorker)
    {
        super(game);
        _game = game;
        _netWorker = netWorker;
        state = GAME_READY;
        _tank = tank;
        guiCam = new Camera2D(glGraphics, 1920, 1080);
        touchPoint = new Vector2();
        batcher = new SpriteBatcher(glGraphics, 600);
        worldListener = new WorldListener()
        {
            @Override
            public void jump()
            {
            }

            public void attack()
            {

            }

            @Override
            public void highJump()
            {
            }

            @Override
            public void hit()
            {
            }

            @Override
            public void coin()
            {
            }
        };
        world = new World(worldListener, tank, netWorker);
        renderer = new WorldRenderer(glGraphics, batcher, world);
        pauseBounds = new Rectangle(320 - 64, 480 - 64, 64, 64);
        moveLeftBounds = new Rectangle(1489, 930, 203, 126);
        moveRightBounds = new Rectangle(1696, 930, 203, 126);
        powerBounds = new Rectangle(467, 931, 336, 144);
        cannonAngleBounds = new Rectangle(0, 826, 400, 245);
        pleeBounds = new Circle(1066, 961, 99);
        resumeBounds = new Rectangle(160 - 96, 240, 192, 36);
        quitBounds = new Rectangle(160 - 96, 240 - 36, 192, 36);
        lastScore = 0;
        scoreString = "score: 0";
        fpsCounter = new FPSCounter();

        _netWorker.getMovePacketReceivedEvent().subscribe(new IEventHandler<MovePacketEventArgs>()
        {
            @Override
            public void handle(Object source, MovePacketEventArgs args)
            {
                world.enemyTank.moveTank(args.MovePacket.dx);
            }
        });
        _netWorker.getAnglePacketReceivedEvent().subscribe(new IEventHandler<AnglePacketEventArgs>()
        {
            @Override
            public void handle(Object source, AnglePacketEventArgs args)
            {
                world.enemyTank.setCannonAngle(args.Packet.Angle);
            }
        });
        _netWorker.getDisconnectedEvent().subscribe(new IEventHandler<EventArgs>()
        {
            @Override
            public void handle(Object source, EventArgs args)
            {
                _game.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Toast.makeText(_game, "Произошла потеря соединения c " + _netWorker.getEnemyName(), Toast.LENGTH_LONG).show();
                    }
                });
                _game.setScreen(new MainMenuScreen(_game));
            }
        });
        _netWorker.getShotPacketReceivedEvent().subscribe(new IEventHandler<ShotPacketEventArgs>()
        {
            @Override
            public void handle(Object source, final ShotPacketEventArgs args)
            {
                _game.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        float power = args.Packet.Power;
                        world.enemyTank.currentPower = power;
                        world.enemyTank.saveShotTank();
                        world.enemyTank.GetTankInfo();
                        NetLogger.log(_netWorker.getEnemyName(), "Выстрел", ActionResult.Success);
                    }
                });
            }
        });
    }

    @Override
    public void update(float deltaTime)
    {
        if (deltaTime > 0.1f) deltaTime = 0.1f;

        switch (state)
        {
            case GAME_READY:
                updateReady();
                break;
            case GAME_RUNNING:
                updateRunning(deltaTime);
                break;
            case GAME_PAUSED:
                updatePaused();
                break;
            case GAME_LEVEL_END:
                updateLevelEnd();
                break;
            case GAME_OVER:
                updateGameOver();
                break;
        }
    }

    private void updateReady()
    {
        if (game.getInput().getTouchEvents().size() > 0)
        {
            state = GAME_RUNNING;
        }
        world.update();
    }

    private void updateRunning(float deltaTime)
    {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        int len = touchEvents.size();
        if (touchLeft)
        {
            _netWorker.sendMovePacket(-1.1f);
            world.myTank.moveTank(-1.1f);
        }
        if (touchRight)
        {
            _netWorker.sendMovePacket(1.1f);
            world.myTank.moveTank(1.1f);
        }
        for (int i = 0; i < len; i++)
        {
            TouchEvent event = touchEvents.get(i);
            if (event.type == TouchEvent.TOUCH_UP)
            {
                touchRight = false;
                touchLeft = false;
            }
            touchPoint.set(event.x, event.y);
            guiCam.touchToWorld(touchPoint);

            if (OverlapTester.pointInRectangle(moveLeftBounds, touchPoint))
            {
                if (event.type == TouchEvent.TOUCH_DOWN)
                {
                    touchLeft = true;
                }
            }
            if (OverlapTester.pointInRectangle(moveRightBounds, touchPoint))
            {
                if (event.type == TouchEvent.TOUCH_DOWN)
                {
                    touchRight = true;
                }
            }
            if (OverlapTester.pointInRectangle(powerBounds, touchPoint))
            {
                int otnX = (int) touchPoint.x - (int) powerBounds.lowerLeft.x;
                float otn = otnX / powerBounds.width;
                if (otn == 0) otn = (float) 0.1;
                world.myTank.savePowerTank(otn);
                world.myTank.GetTankInfo();
            }

            if (OverlapTester.pointInCircle(pleeBounds, touchPoint) && event.type == TouchEvent.TOUCH_UP)
            {
                _netWorker.sendShotPacket(world.myTank.currentPower);
                world.myTank.saveShotTank();
                world.myTank.GetTankInfo();
            }
            if (OverlapTester.pointInRectangle(cannonAngleBounds, touchPoint))
            {
                float cannonAngle = touchPoint.sub(cannonCenter).angle();
                _netWorker.sendAnglePacket(cannonAngle);
                world.myTank.setCannonAngle(cannonAngle);
            }
        }

        world.update();


        if (world.state == World.WORLD_STATE_GAME_OVER)
        {
            state = GAME_OVER;
        }
    }

    private void updatePaused()
    {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        int len = touchEvents.size();
        for (int i = 0; i < len; i++)
        {
            TouchEvent event = touchEvents.get(i);
            if (event.type != TouchEvent.TOUCH_UP) continue;

            touchPoint.set(event.x, event.y);
            guiCam.touchToWorld(touchPoint);

            if (OverlapTester.pointInRectangle(resumeBounds, touchPoint))
            {
                state = GAME_RUNNING;
                return;
            }

            if (OverlapTester.pointInRectangle(quitBounds, touchPoint))
            {
                game.setScreen(new MainMenuScreen(_game));
                return;

            }
        }
    }

    private void updateLevelEnd()
    {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        int len = touchEvents.size();
        for (int i = 0; i < len; i++)
        {
            TouchEvent event = touchEvents.get(i);
            if (event.type != TouchEvent.TOUCH_UP) continue;
            world = new World(worldListener, _tank, _netWorker);
            renderer = new WorldRenderer(glGraphics, batcher, world);
            world.score = lastScore;
            state = GAME_READY;
        }
    }

    private void updateGameOver()
    {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        int len = touchEvents.size();
        for (int i = 0; i < len; i++)
        {
            TouchEvent event = touchEvents.get(i);
            if (event.type != TouchEvent.TOUCH_UP) continue;
            NetLogger.log(_netWorker.getName(), "Игра завершена",ActionResult.Success);
            game.setScreen(new MainMenuScreen(_game));
        }
    }

    @Override
    public void present(float deltaTime)
    {
        GL10 gl = glGraphics.getGL();
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        gl.glEnable(GL10.GL_TEXTURE_2D);

        renderer.render();

        guiCam.setViewportAndMatrices();
        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        batcher.beginBatch(Assets.items);
        switch (state)
        {
            case GAME_READY:
                presentReady();
                break;
            case GAME_RUNNING:
                presentRunning();
                break;
            case GAME_PAUSED:
                presentPaused();
                break;
            case GAME_LEVEL_END:
                presentLevelEnd();
                break;
            case GAME_OVER:
                presentGameOver();
                break;
        }
        batcher.endBatch();
        gl.glDisable(GL10.GL_BLEND);
        fpsCounter.logFrame();
    }

    private void presentReady()
    {
        batcher.drawSprite(160, 240, 192, 32, Assets.ready);
    }

    private void presentRunning()
    {
        batcher.drawSprite(320 - 32, 480 - 32, 64, 64, Assets.pause);
        // Assets.font.drawText(batcher, scoreString, 16, 480 - 20);
    }


    private void presentPaused()
    {
        batcher.drawSprite(160, 240, 192, 96, Assets.pauseMenu);
        Assets.font.drawText(batcher, scoreString, 16, 480 - 20);
    }

    private void presentLevelEnd()
    {
        String topText = "the princess is ...";
        String bottomText = "in another castle!";
        float topWidth = Assets.font.glyphWidth * topText.length();
        float bottomWidth = Assets.font.glyphWidth * bottomText.length();
        Assets.font.drawText(batcher, topText, 160 - topWidth / 2, 480 - 40);
        Assets.font.drawText(batcher, bottomText, 160 - bottomWidth / 2, 40);
    }

    private void presentGameOver()
    {
        batcher.drawSprite(160, 240, 160, 96, Assets.gameOver);
        float scoreWidth = Assets.font.glyphWidth * scoreString.length();
        Assets.font.drawText(batcher, scoreString, 160 - scoreWidth / 2, 480 - 20);
    }

    @Override
    public void pause()
    {
        if (state == GAME_RUNNING) state = GAME_PAUSED;
    }

    @Override
    public void resume()
    {
    }

    @Override
    public void dispose()
    {
        _netWorker.dispose();
    }
}
