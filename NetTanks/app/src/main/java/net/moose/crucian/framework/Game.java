package net.moose.crucian.framework;

/**
 * Интерфейс для инкапсуляции управления игровым процессом
 */
public interface Game
{
    /**
     * Метод, позволяющий получить доступ к пользовательскому вводу
     * @return Объект, характеризующий действия пользователя
     */
    public Input getInput();

    /**
     * Метод для работы с файлами
     * @return Объект, отвечающий за доступ к файловой системе
     */
    public FileIO getFileIO();

    /**
     * Метод для управления построением интерфейса
     * @return Объект, отвечающий за создание изображения на экране
     */
    public Graphics getGraphics();

    /**
     * Метод для управления звуком
     * @return Объект, отвечающий за звуковые эффекты
     */
    public Audio getAudio();

    /**
     * Метод для смены текущего экрана
     * @param screen Новый экран
     */
    public void setScreen(Screen screen);

    /**
     * Метод, позволяющий получить доступ к текущему экрану
     * @return Текущий экран
     */
    public Screen getCurrentScreen();

    /**
     * Метод, позволяющий получить доступ к стартовому экрану
     * @return Стартовый экран
     */
    public Screen getStartScreen();
}