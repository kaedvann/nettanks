package net.moose.crucian.tanks;

import net.moose.crucian.framework.math.Circle;
import net.moose.crucian.framework.math.OverlapTester;
import net.moose.crucian.framework.math.Vector2;
import net.moose.crucian.network.domain.INetWorker;
import net.moose.crucian.utils.ActionResult;
import net.moose.crucian.utils.NetLogger;

import java.util.Random;

/**
 * Класс, инкапсулирующий состояние игрового мира
 */
public class World
{
    /**
     * Код, обозначающий состояние движения мира
     */
    public static final int WORLD_STATE_RUNNING = 0;

    /**
     * Код, обозначающий завершение игры
     */
    public static final int WORLD_STATE_GAME_OVER = 3;
    /**
     * Код, обозначающие состояние выстрела
     */
    public static final int WORLD_STATE_SHOOTING = 2;

    /**
     * Танки игроков
     */
    public final Tank myTank, enemyTank;
    /**
     * Снаряды игроков
     */
    public Charge myCharge, enemyCharge;

    /**
     * Текущее состояние мира
     */
    public int state;
    public final WorldListener listener;
    public final Random rand;
    private final INetWorker _worker;
    public float heightSoFar;
    public int score;
    public static final Vector2 gravity = new Vector2(0, -12);

    public int _tank;

    public World(WorldListener listener, int tank, INetWorker worker)
    {
        _worker = worker;
        myCharge = new Charge();
        enemyCharge = new Charge();
        if (tank == 1)
        {
            this.myTank = new Tank(49, 419);
            myTank.fileName = ".myTank";
            myTank.healthPos = 83;
            this.enemyTank = new Tank(1105, 400);
            enemyTank.fileName = ".enemyTank";
            enemyTank.healthPos = 1449;
        } else
        {
            this.myTank = new Tank(1105, 400);
            myTank.fileName = ".myTank";
            myTank.healthPos = 1449;
            this.enemyTank = new Tank(49, 419);
            enemyTank.fileName = ".enemyTank";
            enemyTank.healthPos = 83;
        }
        myTank.tankName = worker.getName();
        enemyTank.tankName = worker.getEnemyName();
        this.listener = listener;
        rand = new Random();
        generateLevel();
        _tank = tank;
        this.heightSoFar = 0;
        this.score = 0;
        this.state = WORLD_STATE_RUNNING;
    }

    private void generateLevel()
    {

    }

    public void update()
    {
        if (myTank.state == Tank.TANK_STATE_SHOOT)
        {
            state = WORLD_STATE_SHOOTING;
            if (!myCharge.isActive)
            {
                myCharge.isActive = true;
                myCharge.chargeX = myTank.chargeX;
                myCharge.chargeY = myTank.chargeY;
            }

            Shoot(myTank, enemyTank, myCharge, _worker.getEnemyName());
        }
        if (enemyTank.state == Tank.TANK_STATE_SHOOT)
        {
            state = WORLD_STATE_SHOOTING;
            if (!enemyCharge.isActive)
            {
                enemyCharge.isActive = true;
                enemyCharge.chargeX = enemyTank.chargeX;
                enemyCharge.chargeY = enemyTank.chargeY;
            }
            Shoot(enemyTank, myTank, enemyCharge, _worker.getName());
        }
        checkGameOver();
    }




    private void Shoot(Tank tank, Tank enemyTank, Charge charge, String enemyName)
    {
        float G = (float) 9.81;
        float angle = tank.chargeAng;
        float speed = tank.chargePower * 100;
        float tick = 100, timeTick = tick / 1000;

        if (charge.chargeX == 0 && charge.chargeY == 0)
        {
            charge.vX = speed * Math.cos(Math.toRadians(angle));
            charge.vY = speed * Math.sin(Math.toRadians(angle));
            charge.dVx = charge.vX * timeTick;
            charge.dVy = G * timeTick;
        }
        if (charge.chargeY + tank.chargePosY >= enemyTank.position.y || charge.chargeY > 2000)
        {
            charge.chargeX += charge.dVx;
            charge.vY -= charge.dVy;
            charge.chargeY += charge.vY * timeTick;
           // tank.chargeX = chargeX;
           // tank.chargeY = chargeY;
        } else
        {
            tank.state = Tank.TANK_STATE_ACT;
            state = WORLD_STATE_RUNNING;
            charge.clear();
            charge.isActive = false;
        }
        if (OverlapTester.overlapCircleRectangle(new Circle(charge.chargeX + tank.chargePosX,
                charge.chargeY + tank.chargePosY, charge.chargeRad), enemyTank.bounds))
        {
            enemyTank.hit((float) 0.2);
            NetLogger.log(enemyName, String.format("Обработка попадания, осталось %4.1d здоровья", enemyTank.currentHealth), ActionResult.Success);
            if (enemyTank.state == Tank.TANK_STATE_LOOSE)
            {
                NetLogger.log(enemyName, "Поражение", ActionResult.Success);
                state = WORLD_STATE_GAME_OVER;
            }
            charge.clear();
            charge.isActive = false;
            tank.state = Tank.TANK_STATE_ACT;
            state = WORLD_STATE_RUNNING;

        }

    }

    private void checkGameOver()
    {

    }

    private void updateTanks()
    {

    }


//private void checkSquirrelCollisions() {
//    int len = squirrels.size();
//    for (int i = 0; i < len; i++) {
//        Squirrel squirrel = squirrels.get(i);
//        if (OverlapTester.overlapRectangles(squirrel.bounds, bob.bounds)) {
//            bob.hitSquirrel();
//            listener.hit();
//        }
//    }
//}


    public interface WorldListener
    {
        public void jump();

        public void attack();

        public void highJump();

        public void hit();

        public void coin();
    }
}
