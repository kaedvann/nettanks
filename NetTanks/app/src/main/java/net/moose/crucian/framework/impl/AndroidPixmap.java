package net.moose.crucian.framework.impl;

import android.graphics.Bitmap;

import net.moose.crucian.framework.Graphics.PixmapFormat;
import net.moose.crucian.framework.Pixmap;

public class AndroidPixmap implements Pixmap
{
    Bitmap bitmap;
    PixmapFormat format;

    public AndroidPixmap(Bitmap bitmap, PixmapFormat format)
    {
        this.bitmap = bitmap;
        this.format = format;
    }

    @Override
    public int getWidth()
    {
        return bitmap.getWidth();
    }

    @Override
    public int getHeight()
    {
        return bitmap.getHeight();
    }

    @Override
    public PixmapFormat getFormat()
    {
        return format;
    }

    @Override
    public void dispose()
    {
        bitmap.recycle();
    }
}
