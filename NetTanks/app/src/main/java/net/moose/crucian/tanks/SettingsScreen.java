package net.moose.crucian.tanks;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.audiofx.BassBoost;
import android.text.InputType;
import android.widget.EditText;

import net.moose.crucian.framework.Game;
import net.moose.crucian.framework.Input;
import net.moose.crucian.framework.gl.Camera2D;
import net.moose.crucian.framework.gl.Font;
import net.moose.crucian.framework.gl.SpriteBatcher;
import net.moose.crucian.framework.impl.GLScreen;
import net.moose.crucian.framework.math.OverlapTester;
import net.moose.crucian.framework.math.Rectangle;
import net.moose.crucian.framework.math.Vector2;

import java.util.List;

import javax.microedition.khronos.opengles.GL10;

public class SettingsScreen extends GLScreen
{
    Camera2D guiCam;
    SpriteBatcher batcher;
    Rectangle soundBounds;
    Rectangle changeNameBounds;
    Rectangle backBounds, settingsBounds;
    Rectangle helpBounds;
    Vector2 touchPoint;
    TanksGame _game;
    String _input;
    public SettingsScreen(TanksGame game)
    {
        super(game);
        guiCam = new Camera2D(glGraphics, 1920, 1080);
        batcher = new SpriteBatcher(glGraphics, 100);
        soundBounds = new Rectangle(0, 0, 64, 64);
        changeNameBounds = new Rectangle(717, 361, 638, 133);
        backBounds = new Rectangle(786, 900, 509, 100);
        settingsBounds = new Rectangle(646, 631, 662, 182);
        helpBounds = new Rectangle(834, 606, 413, 130);
        touchPoint = new Vector2();
        _game = game;
    }

    @Override
    public void update(float deltaTime)
    {
        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        int len = touchEvents.size();
        for (int i = 0; i < len; i++)
        {
            Input.TouchEvent event = touchEvents.get(i);
            if (event.type == Input.TouchEvent.TOUCH_UP)
            {
                touchPoint.set(event.x, event.y);
                guiCam.touchToWorld(touchPoint);

                if (OverlapTester.pointInRectangle(changeNameBounds, touchPoint))
                {
                    _game.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            final EditText input = new EditText(_game);
                            new AlertDialog.Builder(_game).setTitle("Имя пользователя").setMessage("Введите новое имя пользователя").setView(input).setPositiveButton("Ок", new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int whichButton)
                                {
                                    _input = input.getText().toString();
                                    Settings.login = _input;
                                    // TODO создать клиента и запустить подключение

                                }
                            }).setNegativeButton("Отмена", new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int whichButton)
                                {

                                }
                            }).show();
                        }
                    });
                    return;
                }
                if (OverlapTester.pointInRectangle(helpBounds, touchPoint))
                {
                    game.setScreen(new HelpScreen(game));
                    return;
                }
                if (OverlapTester.pointInRectangle(backBounds, touchPoint))
                {
                    game.setScreen(new MainMenuScreen((TanksGame) game));
                    return;
                }
            }
        }
    }

    @Override
    public void present(float deltaTime)
    {
        GL10 gl = glGraphics.getGL();
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        guiCam.setViewportAndMatrices();

        gl.glEnable(GL10.GL_TEXTURE_2D);

        batcher.beginBatch(Assets.settings);
        batcher.drawSprite(960, 540, 1920, 1080, Assets.settingsRegion);
        batcher.endBatch();

        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        batcher.beginBatch(Assets.items);
        Font f = new Font(Assets.items);
        f.drawText(batcher, Settings.login, 771, 1080-285);
        batcher.endBatch();
        gl.glDisable(GL10.GL_BLEND);
    }

    @Override
    public void pause()
    {
        Settings.save(game.getFileIO());
    }

    @Override
    public void resume()
    {
    }

    @Override
    public void dispose()
    {
    }
}
