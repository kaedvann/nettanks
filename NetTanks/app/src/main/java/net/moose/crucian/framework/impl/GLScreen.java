package net.moose.crucian.framework.impl;

import net.moose.crucian.framework.Game;
import net.moose.crucian.framework.Screen;

public abstract class GLScreen extends Screen
{
    protected final GLGraphics glGraphics;
    protected final GLGame glGame;

    public GLScreen(Game game)
    {
        super(game);
        glGame = (GLGame) game;
        glGraphics = ((GLGame) game).getGLGraphics();
    }

}
