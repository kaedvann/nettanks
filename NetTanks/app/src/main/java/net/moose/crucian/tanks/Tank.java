package net.moose.crucian.tanks;

import net.moose.crucian.framework.DynamicGameObject;
import net.moose.crucian.framework.math.Vector2;

import java.util.LinkedList;

/**
 * Created by Crucian on 23.10.2014.
 */
public class Tank extends DynamicGameObject
{
    public static final int TANK_MAX_HEALTH = 1;
    public static final int TANK_STATE_ACT = 0;
    public static final int TANK_STATE_SHOOT = 1;
    public static final int TANK_STATE_LOOSE = 2;
    public static final float TANK_WIDTH = 260;
    public static final float TANK_HEIGHT = 210;
    public String fileName = ".tank", tankName;
    public float currentHealth; //multiply from 0 to 1
    public Vector2 cannonVect;
    public float cannonAngle;
    public float currentPower; //multiply from 0 to 1
    public float chargeX = 0, chargeY = 0;
    public float chargePosX, chargePosY;
    public float chargePower = 0, chargeAng = 0;
    public int state, healthPos = 0;
    LinkedList<String> queue;
    private int stringNum = 0, prevStringNum = 0;


    public Tank(float x, float y)
    {
        super(x, y, TANK_WIDTH, TANK_HEIGHT);
        currentHealth = TANK_MAX_HEALTH;
        position.x = x;
        position.y = y;
        currentPower = 1;
        queue = new LinkedList<>();
        cannonVect = new Vector2(2, 0);
        velocity.add(11, 0);
    }

    public void setCannonAngle(float ang)
    {
        if (ang > 180 && ang <= 360) ang = 360 - ang;
        cannonAngle = ang;
    }


    // m - move, a - angle changed, s - shot, p - power changed, h - health changed
    public void GetTankInfo()
    {
        // ArrayList<String> lst = Settings.loadTank(fileIO, fileName, prevStringNum, stringNum);
        float moveDelta = 0, angDelta = 0;
        float pleeX = 0, pleeY = 0, pleePower = 0;
        String current;
        while ((current = queue.pollLast()) != null)
        {
            String[] parts = current.split(":");
            switch (parts[0].charAt(0))
            {
                case 'm':
                    moveDelta += Float.parseFloat(parts[1]);
                    moveTank(moveDelta);
                    break;
                case 'a':
                    cannonAngle = Float.parseFloat(parts[1]);
                    break;
                case 's':
                    chargePosX = Float.parseFloat(parts[1]);
                    chargePosY = Float.parseFloat(parts[2]);
                    chargePower = Float.parseFloat(parts[3]);
                    chargeAng = Float.parseFloat(parts[4]);
                    chargeX = 0;
                    chargeY = 0;
                    state = TANK_STATE_SHOOT;
                    break;
                case 'p':
                    currentPower = Float.parseFloat(parts[1]);
                    break;
            }
        }

        prevStringNum = stringNum;
    }

    private void updateBounds()
    {
       // x - width / 2, y - height / 2,
        bounds.lowerLeft.x = position.x - bounds.width/2;
     //   bounds.lowerLeft.y = position.y - bounds.height / 2;
    }

    public void moveTank(float delta)
    {
        position.add(velocity.x * delta, velocity.y * delta);
        updateBounds();
    }


    public Vector2 getCannonPos()
    {
        return (new Vector2(position.x, position.y + 50));
    }


    public void hit(float damage)
    {
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            state = TANK_STATE_LOOSE;
        }
    }


    public void savePowerTank(final float v)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("p:");
        sb.append(v);
        sb.append('\n');
        queue.push(sb.toString());
        //Settings.saveTank(fileIO, fileName, sb.toString());
        stringNum += 1;

    }

    public void saveShotTank()
    {
        float x = 0, y = 0;
        StringBuilder sb = new StringBuilder();
        sb.append("s:");
        sb.append(position.x);
        sb.append(":");
        sb.append(position.y + 50);
        sb.append(":");
        sb.append(currentPower);
        sb.append(":");
        sb.append(cannonAngle);
        sb.append('\n');
        queue.push(sb.toString());
        // Settings.saveTank(fileIO, fileName, sb.toString());
        stringNum += 1;
    }
}
