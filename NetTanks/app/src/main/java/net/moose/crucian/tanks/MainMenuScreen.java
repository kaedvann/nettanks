package net.moose.crucian.tanks;

import net.moose.crucian.framework.Input.TouchEvent;
import net.moose.crucian.framework.gl.Camera2D;
import net.moose.crucian.framework.gl.SpriteBatcher;
import net.moose.crucian.framework.impl.GLScreen;
import net.moose.crucian.framework.math.OverlapTester;
import net.moose.crucian.framework.math.Rectangle;
import net.moose.crucian.framework.math.Vector2;

import java.util.List;

import javax.microedition.khronos.opengles.GL10;

public class MainMenuScreen extends GLScreen
{
    Camera2D guiCam;
    SpriteBatcher batcher;
    Rectangle playBounds;
    Rectangle connectionBounds, settingsBounds;
    Rectangle helpBounds;
    Rectangle cheatCloudBounds;
    Vector2 touchPoint;

    public MainMenuScreen(TanksGame game)
    {
        super(game);
        guiCam = new Camera2D(glGraphics, 1920, 1080);
        batcher = new SpriteBatcher(glGraphics, 100);
        playBounds = new Rectangle(646, 231, 662, 182);
        connectionBounds = new Rectangle(646, 431, 662, 182);
        settingsBounds = new Rectangle(646, 631, 662, 182);
        cheatCloudBounds = new Rectangle(1531, 90, 217, 90);
        helpBounds = new Rectangle(160 - 150, 200 - 18 - 36, 300, 36);
        touchPoint = new Vector2();
    }

    @Override
    public void update(float deltaTime)
    {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();

        for (TouchEvent event : touchEvents)
        {
            if (event.type == TouchEvent.TOUCH_UP)
            {
                touchPoint.set(event.x, event.y);
                guiCam.touchToWorld(touchPoint);

                if (OverlapTester.pointInRectangle(playBounds, touchPoint))
                {
                    game.setScreen(new WaitForConnectionScreen((TanksGame) game));
                    return;
                }
                if (OverlapTester.pointInRectangle(connectionBounds, touchPoint))
                {
                    game.setScreen(new ConnectionScreen((TanksGame) game));
                    return;
                }
                if (OverlapTester.pointInRectangle(settingsBounds, touchPoint))
                {
                    game.setScreen(new SettingsScreen((TanksGame) game));
                    return;
                }
            }
        }
    }

    @Override
    public void present(float deltaTime)
    {
        GL10 gl = glGraphics.getGL();
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        guiCam.setViewportAndMatrices();

        gl.glEnable(GL10.GL_TEXTURE_2D);

        batcher.beginBatch(Assets.background);
        batcher.drawSprite(960, 540, 1920, 1080, Assets.backgroundRegion);
        batcher.endBatch();

        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);

        batcher.beginBatch(Assets.items);
        batcher.drawSprite(971, 517, 551, 548, Assets.mainMenu);
        batcher.endBatch();

        gl.glDisable(GL10.GL_BLEND);
    }

    @Override
    public void pause()
    {
        Settings.save(game.getFileIO());
    }

    @Override
    public void resume()
    {
    }

    @Override
    public void dispose()
    {
    }
}

