package net.moose.crucian.tanks;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.InputType;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import net.moose.crucian.framework.gl.Camera2D;
import net.moose.crucian.framework.gl.SpriteBatcher;
import net.moose.crucian.framework.impl.AndroidInput;
import net.moose.crucian.framework.impl.GLScreen;
import net.moose.crucian.framework.math.Rectangle;
import net.moose.crucian.framework.math.Vector2;
import net.moose.crucian.network.client.BroadcastListener;
import net.moose.crucian.network.client.BroadcastServerEventArgs;
import net.moose.crucian.network.client.NetworkClient;
import net.moose.crucian.utils.ActionResult;
import net.moose.crucian.utils.ExceptionEventArgs;
import net.moose.crucian.utils.NetLogger;

import org.npospelt.events.EventArgs;
import org.npospelt.events.IEventHandler;

import java.io.IOException;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Crucian on 23.10.2014.
 */
public class ConnectionScreen extends GLScreen
{
    private final TanksGame _game;
    Camera2D guiCam;
    SpriteBatcher batcher;
    Rectangle backBounds;
    Vector2 touchPoint;
    TextView textView;
    private boolean _toDispose = true;
    private String _input;
    private NetworkClient _networkClient;
    private BroadcastListener _discoverer;
    private IEventHandler<ExceptionEventArgs<Exception>> _errorEventHandler = new IEventHandler<ExceptionEventArgs<Exception>>()
    {
        @Override
        public void handle(Object source, ExceptionEventArgs<Exception> args)
        {
            _networkClient.getErrorOccurredEvent().unsubscribe(this);
            _discoverer.getErrorOccurred().unsubscribe(this);
            handleError();
            NetLogger.log(Settings.login, "Подключение к серверу", ActionResult.Error);
        }
    };

    public ConnectionScreen(TanksGame game)
    {
        super(game);
        _game = game;
        guiCam = new Camera2D(glGraphics, 1920, 1080);
        backBounds = new Rectangle(0, 0, 64, 64);
        touchPoint = new Vector2();
        batcher = new SpriteBatcher(glGraphics, 100);
        AndroidInput inp = (AndroidInput) glGame.getInput();
        textView = new TextView(glGame);
        textView.setText("Press keys (if you have some)!");
        textView.setOnKeyListener(inp.keyHandler);
        textView.setFocusableInTouchMode(true);
        textView.requestFocus();
        textView.beginBatchEdit();
        createDialog();
    }

    private void createDialog()
    {
        _game.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                final EditText input = new EditText(_game);
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                new AlertDialog.Builder(_game).setTitle("Код подключения").setMessage("Введите код, полученный при создании сеанса игры").setView(input).setPositiveButton("Ок", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        _input = input.getText().toString();
                        // TODO создать клиента и запустить подключение
                        createConnection();
                    }
                }).setNegativeButton("Отмена", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        game.setScreen(new MainMenuScreen(_game));
                    }
                }).show();
            }
        });
    }

    private void createConnection()
    {
        try
        {
            _networkClient = NetworkClient.createClient();
            _discoverer = BroadcastListener.createListener(_game, Settings.login);
            _discoverer.getBroadcastServerDiscovered().subscribe(new IEventHandler<BroadcastServerEventArgs>()
            {
                @Override
                public void handle(Object source, BroadcastServerEventArgs args)
                {
                    _discoverer.stop();
                    _discoverer.getBroadcastServerDiscovered().unsubscribe(this);
                    int code;
                    try
                    {
                        code = Integer.parseInt(_input);
                    }
                    catch (NumberFormatException e)
                    {
                        code = 0;
                    }
                    _networkClient.setName(Settings.login);
                    _networkClient.setConnectionCode(code);
                    _networkClient.connect(args.Address, 6666);
                    _networkClient.getErrorOccurredEvent().subscribe(_errorEventHandler);
                    _networkClient.getConnectedEvent().subscribe(new IEventHandler<EventArgs>()
                    {
                        @Override
                        public void handle(Object source, EventArgs args)
                        {
                            _toDispose = false;
                            game.setScreen(new GameScreen(_game, 2, _networkClient));
                        }
                    });
                    _networkClient.getAuthorizationFailedEvent().subscribe(new IEventHandler<EventArgs>()
                    {
                        @Override
                        public void handle(Object source, EventArgs args)
                        {
                            _game.runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    NetLogger.log(Settings.login, "Проверка кода", ActionResult.Error);
                                    Toast.makeText(_game, "Неверный код", Toast.LENGTH_LONG).show();
                                }
                            });
                            _game.setScreen(new MainMenuScreen(_game));
                        }
                    });
                }
            });
            _discoverer.getErrorOccurred().subscribe(new IEventHandler<ExceptionEventArgs<Exception>>()
            {
                @Override
                public void handle(Object source, ExceptionEventArgs<Exception> args)
                {
                    _game.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Toast.makeText(_game, "Сервер не найден", Toast.LENGTH_LONG).show();
                        }
                    });
                    _game.setScreen(new MainMenuScreen(_game));
                }
            });
            _discoverer.start();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            NetLogger.log("name", "создание сетевого клиента", ActionResult.Error);
            handleError();
        }
    }

    private void handleError()
    {
//        _game.runOnUiThread(new Runnable()
//        {
//            @Override
//            public void run()
//            {
//                Toast.makeText(_game, "Ошибка подключения", Toast.LENGTH_SHORT).show();
//            }
//        });
//        _game.setScreen(new MainMenuScreen(_game));
    }


    @Override
    public void update(float deltaTime)
    {

    }

    @Override
    public void present(float deltaTime)
    {
        GL10 gl = glGraphics.getGL();
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        guiCam.setViewportAndMatrices();

        gl.glEnable(GL10.GL_TEXTURE_2D);

        batcher.beginBatch(Assets.background);
        batcher.drawSprite(960, 540, 1920, 1080, Assets.backgroundRegion);
        batcher.endBatch();

        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        gl.glDisable(GL10.GL_BLEND);
    }

    @Override
    public void pause()
    {
    }

    @Override
    public void resume()
    {
    }

    @Override
    public void dispose()
    {
        if (_toDispose) _networkClient.dispose();
    }
}
