package net.moose.crucian.tanks;

import net.moose.crucian.framework.gl.Camera2D;
import net.moose.crucian.framework.gl.Font;
import net.moose.crucian.framework.gl.SpriteBatcher;
import net.moose.crucian.framework.impl.GLScreen;
import net.moose.crucian.framework.math.Rectangle;
import net.moose.crucian.framework.math.Vector2;
import net.moose.crucian.network.server.ConnectedClientEventArgs;
import net.moose.crucian.network.server.NetworkServer;

import org.npospelt.events.IEventHandler;

import java.io.IOException;
import java.util.Random;

import javax.microedition.khronos.opengles.GL10;

public class WaitForConnectionScreen extends GLScreen
{
    Camera2D guiCam;
    SpriteBatcher batcher;
    Rectangle soundBounds;
    Rectangle playBounds;
    Rectangle highscoresBounds;
    Rectangle helpBounds;
    Vector2 touchPoint;
    NetworkServer _server;
    private boolean _toDispose = true;

    public WaitForConnectionScreen(final TanksGame game)
    {
        super(game);
        guiCam = new Camera2D(glGraphics, 1920, 1080);
        batcher = new SpriteBatcher(glGraphics, 100);
        touchPoint = new Vector2();
        try
        {
            _server = NetworkServer.createServer(game);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        _server.setName(Settings.login);
        Random rnd = new Random();
        _server.setConnectionCode(rnd.nextInt(900000) + 100000);
        _server.getNewClientConnected().subscribe(new IEventHandler<ConnectedClientEventArgs>()
        {
            @Override
            public void handle(Object source, ConnectedClientEventArgs args)
            {
                _toDispose = false;
                game.setScreen(new GameScreen(game, 1, _server));
            }
        });

        _server.start();
    }


    @Override
    public void update(float deltaTime)
    {

    }

    @Override
    public void present(float deltaTime)
    {
        GL10 gl = glGraphics.getGL();
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        guiCam.setViewportAndMatrices();

        gl.glEnable(GL10.GL_TEXTURE_2D);

        batcher.beginBatch(Assets.wait_background);
        batcher.drawSprite(960, 540, 1920, 1080, Assets.waitRegion);
        batcher.endBatch();

        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);

        batcher.beginBatch(Assets.items);
        Font f = new Font(Assets.items);
        f.drawText(batcher, Integer.toString(_server.getConnectionCode()), 530, 1080 - 430);

        batcher.endBatch();

        gl.glDisable(GL10.GL_BLEND);
    }

    @Override
    public void pause()
    {
        Settings.save(game.getFileIO());
    }

    @Override
    public void resume()
    {
    }

    @Override
    public void dispose()
    {
        if (_toDispose) _server.dispose();
    }
}
