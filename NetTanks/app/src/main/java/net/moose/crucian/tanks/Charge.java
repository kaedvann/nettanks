package net.moose.crucian.tanks;

public class Charge
{
    public float chargeX = 0, chargeY = 0, chargeRad = 14;
    public double dVx = 0, dVy = 0, vX = 0, vY = 0;
    public String tank = "my";
    public boolean isActive = false;

    public void clear()
    {
        chargeX = 0; chargeY = 0;
        dVx = 0; dVy = 0; vX = 0; vY = 0;
    }

}
