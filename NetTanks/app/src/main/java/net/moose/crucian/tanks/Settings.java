package net.moose.crucian.tanks;

/**
 * Created by Crucian on 23.10.2014.
 */

import net.moose.crucian.framework.FileIO;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class Settings
{
    public final static int[] highscores = new int[]{100, 80, 50, 30, 10};
    public final static String file = ".nettanks";
    public static boolean soundEnabled = true;
    public static String login = "player1";
    public FileIO fio;

    public static void load(FileIO files)
    {
        BufferedReader in = null;
        try
        {
            in = new BufferedReader(new InputStreamReader(files.readFile(file)));
            soundEnabled = Boolean.parseBoolean(in.readLine());
            for (int i = 0; i < 5; i++)
            {
                highscores[i] = Integer.parseInt(in.readLine());
            }
        }
        catch (IOException e)
        {
            // :( It's ok we have defaults
        }
        catch (NumberFormatException e)
        {
            // :/ It's ok, defaults save our day
        }
        finally
        {
            try
            {
                if (in != null) in.close();
            }
            catch (IOException e)
            {
            }
        }
    }

    public static void save(FileIO files)
    {
        BufferedWriter out = null;
        try
        {
            out = new BufferedWriter(new OutputStreamWriter(files.writeFile(file)));
            out.write(Boolean.toString(soundEnabled));
            out.write("\n");
            for (int i = 0; i < 5; i++)
            {
                out.write(Integer.toString(highscores[i]));
                out.write("\n");
            }

        }
        catch (IOException e)
        {
        }
        finally
        {
            try
            {
                if (out != null) out.close();
            }
            catch (IOException e)
            {
            }
        }
    }

    public static void saveTank(FileIO files, String fileName, String info)
    {
        BufferedWriter out = null;
        try
        {
            out = new BufferedWriter(new OutputStreamWriter(files.writeFile(fileName)));
            out.write(info);

        }
        catch (IOException e)
        {
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.flush();
                    out.close();
                }
            }
            catch (IOException e)
            {
            }
        }
    }

    public static ArrayList<String> loadTank(FileIO files, String fileName, int st, int end)
    {
        BufferedReader in = null;
        ArrayList<String> lst = new ArrayList<>();
        try
        {
            StringBuilder sb = new StringBuilder();

            in = new BufferedReader(new InputStreamReader(files.readFile(fileName)));
            for (int i = 0; i < st; ++i)
            {
                in.readLine();
            }
            for (int i = st; i < end; ++i)
            {
                lst.add(in.readLine());
            }


        }
        catch (IOException e)
        {
            // :( It's ok we have defaults
        }
        catch (NumberFormatException e)
        {
            // :/ It's ok, defaults save our day
        }
        finally
        {
            try
            {
                if (in != null) in.close();
                return lst;
            }
            catch (IOException e)
            {
            }
        }
        return lst;
    }

    public static void addScore(int score)
    {
        for (int i = 0; i < 5; i++)
        {
            if (highscores[i] < score)
            {
                for (int j = 4; j > i; j--)
                    highscores[j] = highscores[j - 1];
                highscores[i] = score;
                break;
            }
        }
    }
}