package net.moose.crucian.tanks;

import net.moose.crucian.framework.Game;
import net.moose.crucian.framework.Input;
import net.moose.crucian.framework.gl.Camera2D;
import net.moose.crucian.framework.gl.Font;
import net.moose.crucian.framework.gl.SpriteBatcher;
import net.moose.crucian.framework.impl.GLScreen;
import net.moose.crucian.framework.math.OverlapTester;
import net.moose.crucian.framework.math.Rectangle;
import net.moose.crucian.framework.math.Vector2;

import java.util.List;

import javax.microedition.khronos.opengles.GL10;

public class HelpScreen extends GLScreen
{
    Camera2D guiCam;
    SpriteBatcher batcher;
    Rectangle backBounds;
    Vector2 touchPoint;

    public HelpScreen(Game game)
    {
        super(game);
        guiCam = new Camera2D(glGraphics, 1920, 1080);
        batcher = new SpriteBatcher(glGraphics, 100);
        backBounds = new Rectangle(639, 805, 646, 104);
        touchPoint = new Vector2();
    }

    @Override
    public void update(float deltaTime)
    {
        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        int len = touchEvents.size();
        for (int i = 0; i < len; i++)
        {
            Input.TouchEvent event = touchEvents.get(i);
            if (event.type == Input.TouchEvent.TOUCH_UP)
            {
                touchPoint.set(event.x, event.y);
                guiCam.touchToWorld(touchPoint);

                if (OverlapTester.pointInRectangle(backBounds, touchPoint))
                {
                    game.setScreen(new SettingsScreen((TanksGame) game));
                    return;
                }
            }
        }
    }

    @Override
    public void present(float deltaTime)
    {
        GL10 gl = glGraphics.getGL();
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        guiCam.setViewportAndMatrices();

        gl.glEnable(GL10.GL_TEXTURE_2D);

        batcher.beginBatch(Assets.help);
        batcher.drawSprite(960, 540, 1920, 1080, Assets.helpRegion);
        batcher.endBatch();


        gl.glDisable(GL10.GL_TEXTURE_2D);
    }

    @Override
    public void pause()
    {
        Settings.save(game.getFileIO());
    }

    @Override
    public void resume()
    {
    }

    @Override
    public void dispose()
    {
    }
}
