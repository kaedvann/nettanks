package net.moose.crucian.tanks;

import net.moose.crucian.framework.DynamicGameObject;

/**
 * Created by Crucian on 23.10.2014.
 */
public class Cannon extends DynamicGameObject
{
    public static final float CANNON_WIDTH = 0.8f;
    public static final float CANNON_HEIGHT = 0.8f;
    public static final float MIN_ANG = 0f;
    public static final float MAX_ANG = 160f;

    public float currentAngle;

    public Cannon(float x, float y)
    {
        super(x, y, CANNON_WIDTH, CANNON_HEIGHT);
        currentAngle = 90;
    }

}
