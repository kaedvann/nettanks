package net.moose.crucian.tanks;

import net.moose.crucian.framework.gl.Camera2D;
import net.moose.crucian.framework.gl.Font;
import net.moose.crucian.framework.gl.SpriteBatcher;
import net.moose.crucian.framework.gl.TextureRegion;
import net.moose.crucian.framework.impl.GLGraphics;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Crucian on 25.10.2014.
 */
public class WorldRenderer
{
    static final float FRUSTUM_WIDTH = 1920;
    static final float FRUSTUM_HEIGHT = 1080;
    GLGraphics glGraphics;
    World world;
    Camera2D cam;
    SpriteBatcher batcher;

    public WorldRenderer(GLGraphics glGraphics, SpriteBatcher batcher, World world)
    {
        this.glGraphics = glGraphics;
        this.world = world;
        this.cam = new Camera2D(glGraphics, FRUSTUM_WIDTH, FRUSTUM_HEIGHT);
        this.batcher = batcher;
    }

    public void render()
    {
        renderBackground();
        renderObjects();
    }

    public void renderBackground()
    {
        batcher.beginBatch(Assets.gameScreen);
        batcher.drawSprite(cam.position.x, cam.position.y, FRUSTUM_WIDTH, FRUSTUM_HEIGHT, Assets.backgroundRegion);
        batcher.endBatch();
    }

    public void renderObjects()
    {
        GL10 gl = glGraphics.getGL();
        Font f = new Font(Assets.items);

        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);

        batcher.beginBatch(Assets.items);
        if (world.state == World.WORLD_STATE_SHOOTING)
        {
            renderCharge();
        }
        renderCannon(world.myTank, Assets.cannon);
        renderCannon(world.enemyTank, Assets.cannonEnemy);
        renderTank(world.myTank, Assets.tank);
        renderTank(world.enemyTank, Assets.tankEnemy);
        f.drawText(batcher, world.myTank.tankName, world.myTank.position.x-80, world.myTank.position.y - 100);
        f.drawText(batcher, world.enemyTank.tankName, world.enemyTank.position.x-80, world.enemyTank.position.y - 100);
        if (world._tank == 1) renderCannonPos(Assets.cannon);
        else renderCannonPos(Assets.cannonEnemy);

        renderHealth(world.myTank, f);
        renderHealth(world.enemyTank, f);
        renderPower(world.myTank);

        batcher.endBatch();
        gl.glDisable(GL10.GL_BLEND);

    }

    private void renderCharge()
    {
        if (world.myCharge.isActive)
            batcher.drawSprite(world.myCharge.chargeX + world.myTank.chargePosX,
                    world.myCharge.chargeY + world.myTank.chargePosY, 20, 20, Assets.charge);
        if (world.enemyCharge.isActive)
            batcher.drawSprite(world.enemyCharge.chargeX + world.enemyTank.chargePosX,
                    world.enemyCharge.chargeY + world.enemyTank.chargePosY, 20, 20, Assets.charge);
    }

    private void renderCannon(Tank tank, TextureRegion keyFrame)
    {
        batcher.drawSprite(tank.position.x, tank.position.y + 70, 146, 56, tank.cannonAngle, keyFrame);
    }

    private void renderTank(Tank tank, TextureRegion keyFrame)
    {
        batcher.drawSprite(tank.position.x, tank.position.y, 280, 229, keyFrame);
    }

    private void renderCannonPos(TextureRegion keyFrame)
    {
        batcher.drawSprite(212, 1080 - 933, 146, 56, world.myTank.cannonAngle, keyFrame);
    }

    private void renderHealth(Tank tank, Font f) // posX - position of healthbar (myTank or enemyTank)
    {
        //x 16,y 29, w335, h59
        int x = (int) (335 * tank.currentHealth); // lenght of healthbar
        batcher.drawSpriteSided(tank.healthPos, 1080 - 59, x, 29, Assets.healthBar);
        f.drawText(batcher, tank.tankName, tank.healthPos+60, 1080 - 100);
    }

    private void renderPower(Tank tank)
    {
        int x = (int) (335 * tank.currentPower);
        batcher.drawSpriteSided(467, 1080 - 994, x, 29, Assets.powerBar);
    }

}
