package org.npospelt.events;

import android.util.Log;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Rostislav on 04.08.2014.
 */
public class EventHolder<TArgs extends EventArgs>
{
    private final List<IEventHandler<TArgs>> _eventHandlers = Collections.synchronizedList(new LinkedList<IEventHandler<TArgs>>());

    public void raiseEvent(Object source, TArgs eventArgs)
    {
        synchronized (_eventHandlers)
        {
            Iterator<IEventHandler<TArgs>> i = _eventHandlers.iterator(); // Must be in synchronized block
            while (i.hasNext())
            {
                try
                {
                    i.next().handle(source, eventArgs);
                }
                catch (Throwable exc)
                {
                    if (BuildConfig.DEBUG)
                        Log.e("Event holder", "Unexpected exception from handler", exc);
                }
            }
        }
    }

    public void subscribe(IEventHandler<TArgs> newHandler)
    {
        if (!_eventHandlers.contains(newHandler)) _eventHandlers.add(newHandler);
        else throw new IllegalStateException("Обработчик уже есть в списке");
    }

    public void unsubscribe(IEventHandler<TArgs> handler)
    {
        _eventHandlers.remove(handler);
    }

}
