package org.npospelt.events;

/**
 * Created by Rostislav on 04.08.2014.
 */
public interface IEventHandler<TArgs extends EventArgs>
{
    void handle(Object source, TArgs args);
}
