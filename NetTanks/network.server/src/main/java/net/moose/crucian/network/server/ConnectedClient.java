package net.moose.crucian.network.server;

import net.moose.crucian.network.domain.AnglePacket;
import net.moose.crucian.network.domain.AuthRequest;
import net.moose.crucian.network.domain.AuthResponse;
import net.moose.crucian.network.domain.MovePacket;
import net.moose.crucian.network.domain.ShotPacket;
import net.moose.crucian.network.domain.eventargs.AnglePacketEventArgs;
import net.moose.crucian.network.domain.eventargs.MovePacketEventArgs;
import net.moose.crucian.network.domain.eventargs.ShotPacketEventArgs;
import net.moose.crucian.utils.ActionResult;
import net.moose.crucian.utils.ExceptionEventArgs;
import net.moose.crucian.utils.NetLogger;

import org.npospelt.events.EventArgs;
import org.npospelt.events.EventHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Crucian on 05.10.2014.
 */
public class ConnectedClient
{
    private final Socket _clientSocket;
    private final String _serverName;
    private final ExecutorService _writingThread = Executors.newSingleThreadExecutor();
    private final ExecutorService _eventThread;
    private final ExecutorService _listeningThread = Executors.newSingleThreadExecutor();
    private final EventHolder<EventArgs> _clientAuthorized = new EventHolder<>();
    private int _correctCode = 0x0011223344;
    private ObjectInputStream _inputReader;
    private ObjectOutputStream _outputWriter;
    private String _name;
    private boolean _isActive = false;
    private Logger _logger = LoggerFactory.getLogger(ConnectedClient.class);
    private EventHolder<MovePacketEventArgs> _movePacketReceivedEvent = new EventHolder<>();
    private EventHolder<ExceptionEventArgs<Exception>> _errorOccurred = new EventHolder<>();
    private EventHolder<EventArgs> _disconnectedEvent = new EventHolder<>();
    private EventHolder<AnglePacketEventArgs> _anglePacketReceivedEvent = new EventHolder<>();
    private EventHolder<ShotPacketEventArgs> _shotPacketReceived = new EventHolder<>();

    public ConnectedClient(Socket clientSocket, String serverName, ExecutorService eventThread)
    {
        _clientSocket = clientSocket;

        _serverName = serverName;
        try
        {
            _clientSocket.setTcpNoDelay(true);
        }
        catch (SocketException e)
        {
            e.printStackTrace();
        }
        _listeningThread.execute(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    handleIncomingMessages();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        });
        _eventThread = eventThread;
    }

    private void handleIncomingMessages() throws IOException
    {
        _inputReader = (new ObjectInputStream(_clientSocket.getInputStream()));
        _isActive = true;
        _outputWriter = new ObjectOutputStream(_clientSocket.getOutputStream());
        while (IsActive())
        {
            try
            {
                Object o = _inputReader.readObject();
                if (o instanceof AuthRequest) handleAuthRequest((AuthRequest) o);
                if (o instanceof MovePacket) handleMovePacket((MovePacket) o);
                if (o instanceof AnglePacket) handleAnglePacket((AnglePacket) o);
                if (o instanceof ShotPacket) handleShotPacket((ShotPacket) o);

            }
            catch (OptionalDataException | ClassNotFoundException e)
            {
                _logger.error("error handling incoming packet", e);
                NetLogger.log(_name, "Разбор входящего пакета", ActionResult.Error);
            }
            catch (Exception e)
            {
                _logger.error("disconnected " + _name, e);
                _eventThread.submit(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        _errorOccurred.raiseEvent(this, new ExceptionEventArgs<Exception>());
                    }
                });
                dispose();
            }
        }
    }

    private synchronized boolean IsActive()
    {
        return _isActive;
    }

    private void handleAuthRequest(AuthRequest o) throws IOException
    {
        _name = o.Name;
        AuthResponse resp = new AuthResponse();
        if (o.Code == _correctCode)
        {
            resp.IsSuccessful = true;
            resp.ServerName = _serverName;
            _outputWriter.writeObject(resp);
            _outputWriter.flush();
            _logger.info("authorized " + _name);
            _clientAuthorized.raiseEvent(this, EventArgs.emptyEventArgs);
        } else
        {
            resp.IsSuccessful = false;
            _outputWriter.writeObject(resp);
            _outputWriter.flush();
            _logger.info("authorization failed " + _name);
            NetLogger.log(getName(), "Авторизация", ActionResult.Error);
            _clientSocket.close();
        }
    }

    private void handleMovePacket(final MovePacket packet)
    {
        NetLogger.log(_name, "Движение", ActionResult.Success);
        _eventThread.submit(new Runnable()
        {
            @Override
            public void run()
            {
                _movePacketReceivedEvent.raiseEvent(this, new MovePacketEventArgs(packet));
            }
        });
    }

    private void handleAnglePacket(final AnglePacket packet)
    {
        NetLogger.log(_name, "Изменение угла", ActionResult.Success);
        _eventThread.submit(new Runnable()
        {
            @Override
            public void run()
            {
                _anglePacketReceivedEvent.raiseEvent(this, new AnglePacketEventArgs(packet));
            }
        });
    }

    private void handleShotPacket(final ShotPacket packet)
    {
        NetLogger.log(_name, "Выстрел", ActionResult.Success);
        _eventThread.submit(new Runnable()
        {
            @Override
            public void run()
            {
                _shotPacketReceived.raiseEvent(this, new ShotPacketEventArgs(packet));
            }
        });
    }

    public void dispose()
    {
        try
        {
            _isActive = false;
            _clientSocket.close();
            _inputReader.close();
            _outputWriter.close();
        }
        catch (Exception e)
        {
            _logger.error("disposing error", e);
        }
    }

    public String getName()
    {
        return _name;
    }

    public EventHolder<EventArgs> getClientAuthorized()
    {
        return _clientAuthorized;
    }

    public EventHolder<ShotPacketEventArgs> getShotPacketReceivedEvent()
    {
        return _shotPacketReceived;
    }

    public EventHolder<MovePacketEventArgs> getMovePacketReceivedEvent()
    {
        return _movePacketReceivedEvent;
    }

    public void sendMovePacket(final float dx)
    {
        sendPacket(new MovePacket(dx));
    }

    private void sendPacket(final Object packet)
    {
        _writingThread.submit(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    _outputWriter.writeObject(packet);
                    _outputWriter.flush();
                }
                catch (final IOException e)
                {
                    e.printStackTrace();
                    _eventThread.execute(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            _errorOccurred.raiseEvent(ConnectedClient.this, new ExceptionEventArgs<Exception>(e));
                        }
                    });
                }
            }
        });
    }

    public void setCode(int connectionCode)
    {
        _correctCode = connectionCode;
    }

    public void sendAnglePacket(float cannonAngle)
    {
        sendPacket(new AnglePacket(cannonAngle));
    }

    public EventHolder<AnglePacketEventArgs> getAnglePacketReceivedEvent()
    {
        return _anglePacketReceivedEvent;
    }

    public void sendShotPacket(float power)
    {
        sendPacket(new ShotPacket(power));
    }

    public EventHolder<ExceptionEventArgs<Exception>> getErrorOccuredEvent()
    {
        return _errorOccurred;
    }
}
