package net.moose.crucian.network.server;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import net.moose.crucian.network.domain.DiscoveryPacket;
import net.moose.crucian.network.domain.INetWorker;
import net.moose.crucian.network.domain.eventargs.AnglePacketEventArgs;
import net.moose.crucian.network.domain.eventargs.MovePacketEventArgs;
import net.moose.crucian.network.domain.eventargs.ShotPacketEventArgs;
import net.moose.crucian.utils.ActionResult;
import net.moose.crucian.utils.ExceptionEventArgs;
import net.moose.crucian.utils.NetLogger;

import org.npospelt.events.EventArgs;
import org.npospelt.events.EventHolder;
import org.npospelt.events.IEventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteOrder;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Crucian on 05.10.2014.
 */
public class NetworkServer implements INetWorker
{
    private static final int BroadcastPort = 6667;
    private static final int MainPort = 6666;
    private final Logger _logger = LoggerFactory.getLogger(NetworkServer.class);
    private final Context _context;
    private ConnectedClient _client;
    private int _connectionCode = 0;
    private ExecutorService _mainServerThread = Executors.newSingleThreadExecutor();
    private ExecutorService _broadcastThread = Executors.newSingleThreadExecutor();
    private ExecutorService _eventThread = Executors.newSingleThreadExecutor();
    private volatile boolean _isActive = false;
    private ServerSocket _listeningSocket;
    private String _name;

    private EventHolder<ConnectedClientEventArgs> _newClientConnected = new EventHolder<>();
    private volatile boolean _broadcastActive;
    private EventHolder<MovePacketEventArgs> _movePacketReceivedEvent = new EventHolder<>();
    private EventHolder<EventArgs> _disconnected = new EventHolder<>();
    private EventHolder<AnglePacketEventArgs> _anglePacketReceived = new EventHolder<>();
    private EventHolder<ShotPacketEventArgs> _shotPacketReceived = new EventHolder<>();

    private NetworkServer(ServerSocket listeningSocket, final Context context)
    {
        _listeningSocket = listeningSocket;
        _context = context;
    }

    public static NetworkServer createServer(Context context) throws IOException
    {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();

        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN))
        {
            ipAddress = Integer.reverseBytes(ipAddress);
        }

        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();
        ServerSocket sock = new ServerSocket(MainPort, 0, InetAddress.getByAddress(ipByteArray));
        return new NetworkServer(sock, context);
    }

    public EventHolder<ConnectedClientEventArgs> getNewClientConnected()
    {
        return _newClientConnected;
    }

    public synchronized void start()
    {
        _isActive = true;
        _mainServerThread.submit(new Runnable()
        {
            @Override
            public void run()
            {
                serverLoop();
            }
        });
        _broadcastThread.submit(new Runnable()
        {
            @Override
            public void run()
            {
                broadcastingLoop();
            }
        });
        _logger.info("server started");
    }

    private void serverLoop()
    {
        while (isActive())
        {
            try
            {
                Socket newClientSocket = _listeningSocket.accept();
                ConnectedClient newClient = new ConnectedClient(newClientSocket, _name, _eventThread);
                NetLogger.log("Неизвестно", "Подключился новый клиент", ActionResult.Success);
                newClient.setCode(_connectionCode);
                newClient.getClientAuthorized().subscribe(_authorizedHandler);
                _client = newClient;
            }
            catch (Exception exc)
            {
                _logger.error("server acepting loop: ", exc);
            }
        }
    }

    /**
     * Метод, в котором происходит широковещательная рассылка пакетов для обнаружения сервера
     */
    private void broadcastingLoop()
    {
        try
        {
            _broadcastActive = true;
            InetAddress broadcastAddress = getBroadcastAddress();
            DatagramSocket sock = new DatagramSocket(BroadcastPort);
            sock.setBroadcast(true);
            DiscoveryPacket pack = new DiscoveryPacket(); //пакет с приветствием
            pack.Message = getName();
            byte[] data = pack.getData();
            DatagramPacket packet = new DatagramPacket(data, data.length, broadcastAddress, BroadcastPort);
            _logger.info("broadcast started");
            NetLogger.log(_name, "Начата широковещательная рассылка", ActionResult.Success);
            while (isBroadcastActive())
            {
                sock.send(packet);
                Thread.sleep(2 * 1000);
            }
            NetLogger.log(_name, "Завершена широковещательная рассылка", ActionResult.Success);
            sock.close();
        }
        catch (Exception e)
        {
            //не удалось начать рассылку
            _logger.error("error starting broadcast", e);
            NetLogger.log(_name, "Начата широковещательная рассылка", ActionResult.Error);
        }
    }

    public synchronized boolean isActive()
    {
        return _isActive;
    }

    InetAddress getBroadcastAddress() throws IOException
    {
        WifiManager wifi = (WifiManager) _context.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcp = wifi.getDhcpInfo();
        // handle null somehow

        int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
        return InetAddress.getByAddress(quads);

    }

    private EventHolder<ExceptionEventArgs<Exception>> _errorOccurred = new EventHolder<>();
    private IEventHandler<EventArgs> _authorizedHandler = new IEventHandler<EventArgs>()
    {
        @Override
        public void handle(Object source, EventArgs args)
        {
            final ConnectedClient client = (ConnectedClient) source;
            ConnectedClientEventArgs clientEventArgs = new ConnectedClientEventArgs();
            clientEventArgs.Name = client.getName();
            _newClientConnected.raiseEvent(this, clientEventArgs);
            client.getClientAuthorized().unsubscribe(_authorizedHandler);
            _broadcastActive = false;

            NetLogger.log(_client.getName(), "Авторизован", ActionResult.Success);

            client.getMovePacketReceivedEvent().subscribe(new IEventHandler<MovePacketEventArgs>()
            {
                @Override
                public void handle(final Object source, final MovePacketEventArgs args)
                {
                    _movePacketReceivedEvent.raiseEvent(source, args);
                }
            });

            client.getAnglePacketReceivedEvent().subscribe(new IEventHandler<AnglePacketEventArgs>()
            {
                @Override
                public void handle(Object source, AnglePacketEventArgs args)
                {
                    _anglePacketReceived.raiseEvent(source, args);
                }
            });

            client.getShotPacketReceivedEvent().subscribe(new IEventHandler<ShotPacketEventArgs>()
            {
                @Override
                public void handle(Object source, ShotPacketEventArgs args)
                {
                    _shotPacketReceived.raiseEvent(source, args);
                }
            });

            _client.getErrorOccuredEvent().subscribe(new IEventHandler<ExceptionEventArgs<Exception>>()
            {
                @Override
                public void handle(Object source, ExceptionEventArgs<Exception> args)
                {
                    _client.getErrorOccuredEvent().unsubscribe(this);
                    _disconnected.raiseEvent(source, args);
                    NetLogger.log(_name, "Проверка соединения", ActionResult.Error);
                }
            });

            new Handler(Looper.getMainLooper()).post(new Runnable()
            {
                @Override
                public void run()
                {
                    Toast.makeText(_context, "Подключен клиент " + client.getName(), Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    public boolean isBroadcastActive()
    {
        return _broadcastActive;
    }

    public synchronized void stop()
    {
        _isActive = false;
        _broadcastActive = false;
    }

    public int getConnectionCode()
    {
        return _connectionCode;
    }

    public void setConnectionCode(int code)
    {
        _connectionCode = code;
    }

    @Override
    public EventHolder<ShotPacketEventArgs> getShotPacketReceivedEvent()
    {
        return _shotPacketReceived;
    }

    @Override
    public EventHolder<MovePacketEventArgs> getMovePacketReceivedEvent()
    {
        return _movePacketReceivedEvent;
    }

    @Override
    public EventHolder<EventArgs> getDisconnectedEvent()
    {
        return _disconnected;
    }

    @Override
    public EventHolder<AnglePacketEventArgs> getAnglePacketReceivedEvent()
    {
        return _anglePacketReceived;
    }

    @Override
    public void sendMovePacket(float dx)
    {
        _client.sendMovePacket(dx);
    }

    public String getName()
    {
        return _name;
    }

    @Override
    public String getEnemyName()
    {
        return _client.getName();
    }

    @Override
    public void dispose()
    {
        try
        {
            _client.dispose();
            _isActive = false;
            _listeningSocket.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void sendShotPacket(float power)
    {
        _client.sendShotPacket(power);
    }

    @Override
    public void sendAnglePacket(float cannonAngle)
    {
        _client.sendAnglePacket(cannonAngle);
    }

    public void setName(final String name)
    {
        _name = name;
    }

    public EventHolder<ExceptionEventArgs<Exception>> getErrorOccurred()
    {
        return _errorOccurred;
    }
}
