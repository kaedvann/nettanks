package net.moose.crucian.network.server;

import org.npospelt.events.EventArgs;

/**
 * Created by Rostislav on 08.10.2014.
 */
public class ConnectedClientEventArgs extends EventArgs
{
    public String Name;
}
