package net.moose.crucian.utils;

public enum ActionResult
{
    Success,
    Error,
    Unknown;

    public String getDescription()
    {
        switch (this)
        {
            case Success:
                return "Успех";
            case Error:
                return "Ошибка";
            default:

                return "Неизвестно";
        }
    }
}
