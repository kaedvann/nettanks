package net.moose.crucian.utils;

import org.npospelt.events.EventArgs;

public class ExceptionEventArgs<T extends Throwable> extends EventArgs
{
    public T Exception;

    public ExceptionEventArgs(T exception)
    {
        Exception = exception;
    }

    public ExceptionEventArgs()
    {
    }
}
