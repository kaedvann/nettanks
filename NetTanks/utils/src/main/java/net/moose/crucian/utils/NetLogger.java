package net.moose.crucian.utils;

import android.os.Environment;

import org.joda.time.DateTime;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class NetLogger
{
    private static String LogPath = Environment.getExternalStorageDirectory().getPath() + "/logs/nettanks/netlog.txt";

    private static OutputStreamWriter _file;

    static
    {
        try
        {
            _file = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(LogPath)));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private NetLogger()
    {

    }

    public static synchronized void log(String login, String action, ActionResult result)
    {
        DateTime dateTime = DateTime.now();
        try
        {
            _file.write(String.format("[%s][%s] %s %s %s\n", dateTime.toLocalDate(), dateTime.toLocalTime(), login, action, result.getDescription()));
            _file.flush();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
