﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace PacketWriter
{
    static class Program
    {
        static void Main(string[] args)
        {
            IPEndPoint endPoint = null;
            byte[] buffer;
            var listener = new UdpClient();
            listener.EnableBroadcast = true;
            listener.Client.Bind(new IPEndPoint(IPAddress.Any, 8080));
            buffer = listener.Receive(ref endPoint);
            var stream = new FileStream("Angle", FileMode.Create);
            stream.Write(buffer, 0, buffer.Length);
            stream.Flush(true);
            stream.Close();
            buffer = listener.Receive(ref endPoint);
            stream = new FileStream("AuthRequest", FileMode.Create);
            stream.Write(buffer, 0, buffer.Length);
            stream.Flush(true);
            stream.Close();

            buffer = listener.Receive(ref endPoint);
            stream = new FileStream("AuthResponse", FileMode.Create);
            stream.Write(buffer, 0, buffer.Length);
            stream.Flush(true);
            stream.Close();

            buffer = listener.Receive(ref endPoint);
            stream = new FileStream("DiscoveryPacket", FileMode.Create);
            stream.Write(buffer, 0, buffer.Length);
            stream.Flush(true);
            stream.Close();

            buffer = listener.Receive(ref endPoint);
            stream = new FileStream("MovePacket", FileMode.Create);
            stream.Write(buffer, 0, buffer.Length);
            stream.Flush(true);
            stream.Close();

            buffer = listener.Receive(ref endPoint);
            stream = new FileStream("ShotPacket", FileMode.Create);
            stream.Write(buffer, 0, buffer.Length);
            stream.Flush(true);
            stream.Close();

        }
    }
}
